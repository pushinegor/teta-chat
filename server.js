import {WebSocketServer} from 'ws';
import {v4 as uuid} from 'uuid';

const clients = {};
const messages = [];

console.log('Server is up and running');

const wss = new WebSocketServer({port: 8000});

wss.on('connection', (ws) => {
	const id = uuid();
	clients[id] = ws;

	ws.send(JSON.stringify({'user': id}));
	ws.send(JSON.stringify(messages));

	ws.on('message', (rawMessage) => {
		const {user, message} = JSON.parse(rawMessage);
		let date = new Date;
		messages.push({user, date, message});
		for (const id in clients) {
			clients[id].send(JSON.stringify([{user, date, message}]))
		}

		console.log({messages})
	})

	ws.on('close', () => {
		delete clients[id];
	})
});
