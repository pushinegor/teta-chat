import React from 'react';
import classNames from 'classnames/bind';
import { Avatar, Indicator, Paragraph, Title } from 'shared/ui';
import Photo from 'shared/img/avatar.png';

import styles from './chat-list.module.scss';

const cn = classNames.bind(styles);

interface IChatListItem {
	className?: string;
}

export const ChatListItem = ({ className }: IChatListItem) => {

	return (
		<button className={cn('item', className)}>
			<div className={cn('item__inner')}>
				<Avatar imgSrc={Photo} title="Лёня" className={cn('avatar')}>
					<Indicator isOnline={true} className={cn('item-indicator')}/>
				</Avatar>

				<div className={cn('item__content')}>
					<div className={cn('item__header')}>
						<Title as="p" className={cn('item__title')}>O'Keefe</Title>
						<Paragraph level={2} className={cn('time')}>11:23 AM</Paragraph>
					</div>

					<Paragraph level={2} className={cn('message')}>Lorem ipsum dolor sit amet..</Paragraph>
				</div>
			</div>
		</button>
	);
};
