import React from 'react';
import classNames from 'classnames/bind'

import { Avatar, Indicator, Title } from "shared/ui";
import Photo from 'shared/img/avatar.png';

import styles from './user-short-info.module.scss';

const cn = classNames.bind(styles);

interface IUserShortInfo {

}

export const UserShortInfo = (props: IUserShortInfo) => {

	return (
		<div className={cn('container')}>
			<div className={cn('inner')}>
				<Avatar imgSrc={Photo} title="Лёня" size={42} />
				<Title as="p" className={cn('title')}>O'Keefe</Title>
				<Indicator isOnline={true}/>
			</div>
		</div>
	);
};
