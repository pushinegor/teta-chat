import React, { useState } from 'react';
import classNames from 'classnames/bind'

import styles from './chat.module.scss';
import { MessageList } from "modules/chat/ui/message-list";
import { ChatTextField } from "modules/chat/ui/chat-text-field";
import { IMessageRequest, IMessagesResponse } from "modules/chat/model";

const cn = classNames.bind(styles);

interface IChatComponent {
	data?: IMessagesResponse;
	onSendMessage: (values: IMessageRequest) => void;
	userId: string;
}

export const ChatComponent = ({data, onSendMessage, userId}: IChatComponent) => {
	const [value, setValue] = useState('');
	const sendMessage = (values: IMessageRequest) => {
		onSendMessage({user: userId, message: values.message});
		setValue('');
	}

	return (
		<div className={cn('container')}>
			<MessageList data={data} userId={userId}/>

			<ChatTextField
				value={value}
				onChange={setValue}
				autoFocus
				disabled={!value?.length}
				onSend={(message) => sendMessage({user: userId, message})}
			/>
		</div>
	);
};
