import React from 'react';

import { ChatComponent } from "modules/chat/ui/chat/chat.component";
import { useChatData } from "modules/chat/model";


export const Chat = () => {
	const {userId, messages, sendMessage} = useChatData();

	if (!userId) return null;

	return (
		<ChatComponent
			userId={userId}
			data={messages}
			onSendMessage={sendMessage}
		/>
	);
};
