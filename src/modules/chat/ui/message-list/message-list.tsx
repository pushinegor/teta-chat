import React from 'react';
import classNames from 'classnames/bind'

import styles from './message-list.module.scss';
import { Message } from "modules/chat/ui/message-list/message";
import { IMessagesResponse } from "modules/chat/model";

const cn = classNames.bind(styles);

interface IMessageList {
	data?: IMessagesResponse;
	userId: string;
}

export const MessageList = ({ data, userId }: IMessageList) => {

	return (
		<div className={cn('list')}>
			{data?.map((item) => (
				<Message
					key={new Date(item.date).getTime()}
					text={item.message}
					time={new Date(item.date)}
					isMine={ item.user === userId }
				/>
			))}
		</div>
	);
};
