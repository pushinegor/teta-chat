import React from 'react';
import classNames from 'classnames/bind'

import styles from './message-list.module.scss';
import { Paragraph } from "shared/ui";

const cn = classNames.bind(styles);

interface IMessage {
	text: string;
	time: Date;
	isMine?: boolean;
}

const formatAMPM = (date: Date) => {
	let hours = date.getHours();
	let minutes: string | number = date.getMinutes();
	const ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : `${minutes}`;

	return `${hours}: ${minutes} ${ampm}`;
};

export const Message = ({text, time, isMine}: IMessage) => {

	return (
		<div className={cn('message', isMine && 'my')} data-testId="message">
			<Paragraph className={cn('message__text')}>{text}</Paragraph>
			<Paragraph level={2} className={cn('message__time')}>
				{ formatAMPM(time) }
			</Paragraph>
		</div>
	);
};
