import React from 'react';
import classNames from 'classnames/bind'
import TextareaAutosize from "react-textarea-autosize";
import SendIcon from 'shared/img/earth.svg';


import styles from './chat-text-field.module.scss';

const cn = classNames.bind(styles);

export interface IChatTextField {
	name?: string;
	placeholder?: string;
	value: string;
	onChange: (value: string) => void;
	onSend: (value: string) => void;
	autoFocus?: boolean;
	disabled?: boolean;
}

export const ChatTextField = ({onChange, autoFocus, value, onSend, disabled, ...rest}: IChatTextField) => {

	return (
		<div className={cn('container')} data-testid="text-field-wrap">
			<div className={cn('inner')}>
				<div className={cn('text-area-wrap')}>
					<TextareaAutosize
						{...rest}
						value={value}
						disabled={false}
						className={cn('text-area')}
						onChange={e => onChange?.(e.target.value)}
						autoFocus={autoFocus}
						minRows={1}
						maxRows={5}
						data-testId="text-area"
					/>

					<button
						className={cn('button')}
						disabled={disabled}
						onClick={() => onSend(value)}
						data-testId="submit-button"
					>
						<img src={SendIcon} alt="отправить сообщение"/>
					</button>
				</div>
			</div>
		</div>
	);
};
