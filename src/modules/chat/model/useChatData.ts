import { useMemo, useState } from 'react';

export interface IMessageRequest {
	user: string;
	message: string;
}

export type IMessagesResponse = {
	user: string;
	date: Date;
	message: string;
}[];


export const useChatData = () => {
	const socket = useMemo(() => new WebSocket("ws://127.0.0.1:8000"), []);
	const [userId, setUserId] = useState<string | undefined>();
	const [messages, setMessages] = useState<IMessagesResponse>([]);

	socket.onopen = () => {
		console.log('WebSocket Connected')
	}

	socket.onmessage = ({ data }) => {
		const response = JSON.parse(data);

		if ('user' in response) {
			setUserId(response.user);
		}

		if (Array.isArray(response)) {
			console.log(messages)
			setMessages([...messages, ...response]);
		}
	}

	const sendMessage = (data: IMessageRequest) => {
		socket.send(JSON.stringify(data));
	}

	return {
		userId,
		messages,
		sendMessage
	}
}
