import React from 'react';
import classNames from "classnames/bind";

import styles from './chat-layout.module.scss';
import { Container } from "shared/ui";

const cn = classNames.bind(styles);


interface IChatLayout {
	sidebarContent: React.ReactNode;
	children: React.ReactNode;
}

export const ChatLayout = ({sidebarContent, children}: IChatLayout) => {

	return (
		<div className={cn('page')}>
			<Container>
				<div className={cn('chat-wrap')}>
					<div className={cn('sidebar')}>
						{sidebarContent}
					</div>

					<div className={cn('chat')}>
						{children}
					</div>
				</div>
			</Container>
		</div>
	);
};
