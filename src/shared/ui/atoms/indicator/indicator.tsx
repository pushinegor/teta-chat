import React from 'react';
import classNames from 'classnames/bind'

import styles from './indicator.module.scss';

const cn = classNames.bind(styles);

interface IIndicator {
	isOnline: boolean;

	className?: string;
}

export const Indicator = ({isOnline, className}: IIndicator) => {

	return (
		<div className={cn('indicator', className, isOnline ? 'online' : 'offline')}/>
	);
};
