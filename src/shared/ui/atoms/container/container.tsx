import React from 'react';
import classNames from "classnames/bind";

import styles from './container.module.scss';

const cn = classNames.bind(styles);

interface IContainer {
	children: React.ReactNode;
	className?: string;
}

export const Container = ({children, className}: IContainer) => {

	return (
		<div className={cn('container', className)}>{children}</div>
	);
};
