import React from 'react';
import classNames from "classnames/bind";

import styles from './paragraph.module.scss';

const cn = classNames.bind(styles);

interface IParagraph {
	level: 1 | 2;
	weight: 400 | 700;
	children: React.ReactNode;

	className?: string;
}

export const Paragraph = ({level, children, className, weight}: IParagraph) => {

	return (
		<p className={cn(
			'paragraph',
			`p${level}`,
			weight === 700 && 'bold',
			className)
		}>{children}</p>
	);
};

Paragraph.defaultProps = {
	level: 1,
	weight: 400
}
