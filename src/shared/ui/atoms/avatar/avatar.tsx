import React from 'react';
import classNames from 'classnames/bind'

import styles from './avatar.module.scss';

const cn = classNames.bind(styles);

interface IAvatar {
	imgSrc?: string;
	title?: string;

	size: number;

	children?: React.ReactNode;
	className?: string;
}

export const Avatar = ({imgSrc, title, size, children, className}: IAvatar) => {
	return (
		<div className={className}>
			<div className={cn('container')} style={{width: size, height: size}}>
					{!!imgSrc && (
						<img src={imgSrc} alt={title || ''} className={cn('image')}/>
					)}
			</div>
			{children}
		</div>
	);
};

Avatar.defaultProps = {
	size: 40
};
