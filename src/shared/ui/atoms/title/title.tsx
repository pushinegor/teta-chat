import React from 'react';
import classNames from 'classnames/bind'

import styles from './title.module.scss';

const cn = classNames.bind(styles);

interface ITitle {
	as: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p';
	type: 'regular' | 'bold';
	children: React.ReactNode;

	className?: string;
}

const Component = ({ children, as: Tag, type, className, }: ITitle) => (
	<Tag className={className}>
		{children}
	</Tag>
);

export const Title = ({ as, className, children, ...rest }: ITitle) => {

	return (
		<Component {...rest} as={as} className={cn(className, as, rest.type, 'title')}>
			{children}
		</Component>
	);
};

Title.defaultProps = {
	as: 'h1',
	type: 'bold'
};
