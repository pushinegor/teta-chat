import React from 'react';
import { ChatLayout } from "shared/ui";
import { ChatList } from "modules/chat-and-user-common";
import { UserShortInfo } from "modules/user";
import { Chat } from "modules/chat";


export const IndexPage = () => {
	return (
		<ChatLayout
			sidebarContent={<ChatList />}
		>
			<UserShortInfo />

			<Chat />
		</ChatLayout>
	);
};
