import React from 'react'
import { ChatTextField, IChatTextField } from 'modules/chat'
import { render, screen, waitFor, fireEvent } from '@testing-library/react'

const commonProps: IChatTextField = {
	name: 'some-name',
	value: 'Hello my dear friend!',
	autoFocus: true,
	placeholder: 'some placeholder',
	onChange: () => {},
	onSend: () => {}
}

test('text field renders correctly', async () => {
	render(<ChatTextField {...commonProps}/>);

	const submitButton = await screen.findByTestId('submit-button');

	await screen.findByText('Hello my dear friend!');
	await screen.findByTestId('text-field-wrap');
	await waitFor(() => expect(submitButton).toBeEnabled());
});

test('User can\'t submit if \"disabled\" prop passed', async () => {
	render(<ChatTextField {...commonProps} disabled/>);

	const submitButton = await screen.findByTestId('submit-button');

	await waitFor(() => expect(submitButton).toBeDisabled());
});
