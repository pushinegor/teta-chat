import React, { useState } from 'react'
import { renderHook, act } from '@testing-library/react-hooks'
import { ChatComponent } from 'modules/chat/ui/chat/chat.component'
import { render, screen, waitFor, fireEvent, findAllByTestId } from '@testing-library/react'
import { IMessagesResponse } from "modules/chat";
import { useMassagesTestHook } from "tests/utils/use-massages-test-hook";


const initialData: IMessagesResponse = [
	{
		user: 'ea749658-6028-46a1-8751-b57268296c10',
		date: new Date('2022-12-24T17:12:47.047Z'),
		message: 'How are you?'
	},
	{
	   user: 'ea749658-6028-46a1-8751-b57268296c10',
	   date: new Date('2022-12-24T17:36:33.574Z'),
	   message: 'Hello!\n'
	}
]

test('ChatComponent works correctly', async () => {
	const { result } = renderHook(() => useMassagesTestHook(initialData));

	const chat = () => (
		<ChatComponent
			data={result.current.messages}
			userId="ea749658-6028-46a1-8751-b57268296c11"
			onSendMessage={(values) => {
				act(() => result.current.setMessages([
					...result.current.messages,
					{
						...values,
						date: new Date()
					}
				]));
			}}
		/>
	)

	const {rerender} = render(chat());

	const messages = await screen.findAllByTestId('message');
	const textArea = await screen.findByTestId('text-area');
	const submitButton = await screen.findByTestId('submit-button');

	await waitFor(() => expect(messages).toHaveLength(2));

	fireEvent.change(textArea, { target: { value: 'Hello my dear!' } });
	expect(textArea).toHaveValue('Hello my dear!');

	fireEvent.click(submitButton);
	expect(textArea).toHaveValue('');

	rerender(chat());
	const newMessages = await screen.findAllByTestId('message');
	expect(newMessages).toHaveLength(3);
});
