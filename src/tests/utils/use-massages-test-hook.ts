import { useCallback, useState } from "react";
import { IMessagesResponse } from "modules/chat";

export const useMassagesTestHook = (initialValue: IMessagesResponse) => {
	const [messages, setMessages] = useState(initialValue);

	return {messages, setMessages}
}
