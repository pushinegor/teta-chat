import React from 'react';
import { Route, BrowserRouter, Routes } from "react-router-dom";
import { IndexPage } from "pages/index.page";

function App() {
  return (
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<IndexPage/>}/>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
